const fs = require('fs');
const path = require('path');

module.exports.uploadBuffer = (fileName,buff) => {
    return new Promise(resolve=>{
        fs.writeFile(`./public/png/${fileName}`, buff, err => { 
            if(err) throw err;
            resolve();
        });
    })
}