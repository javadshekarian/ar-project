const os = require('os');
const cluster = require('cluster');
const chalk = require('chalk');

const {app} = require('./app');

const numCPUs = os.cpus().length;

const colorCode = chalk.rgb(0,200,255);

if(cluster.isMaster){
    console.log(chalk.blue(`Master prcess ${process.pid} is runnig!\n`));

    for(i=0;i<numCPUs;++i){
        cluster.fork();
    }

    cluster.on('exit',(worker,code,single)=>{
        console.log(chalk.red(`Worker process ${worker.process.pid} is died. Restarting ...\n`));
        cluster.fork();
    })
}else{
    app.listen(process.env.PORT||3000,()=>{
        console.log(colorCode(`Worker process pid is: ${process.pid}`));
        console.log(chalk.bold.green(`the app is listen to port ${process.env.PORT||3000}!\n`));
    })
}