import * as THREE from 'three';
import { OrbitControls } from 'OrbitControls';
import { USDZExporter } from 'USDZExporter';
import { GLTFExporter } from 'GLTFExporter';
import { RoomEnvironment } from 'RoomEnvironment.js';
import { GLTFLoader } from 'GLTFLoader.js';
import { OBJExporter } from 'OBJExporter';
import { STLExporter } from 'STLExporter';
import { PLYExporter } from 'PLYExporter';

const input = document.querySelector('input');
const downloadingTheFile = document.getElementById('downloadingTheFile');
// const cube = document.getElementById('cube');

var png_file_name = '';

input.onchange = () => {
    const file = input.files[0];

    const formdata = new FormData();
    formdata.append('file',file);

    const http = new XMLHttpRequest();
    http.onloadend = e => {
        const res = JSON.parse(e.target.responseText);
        png_file_name = res.fileName;

        var img = document.createElement('img');
        img.src = `png/${res.fileName}`;
        document.body.appendChild(img);

        init();
    }
    http.open('post','/remove-background');
    http.send(formdata);
}

var camera,scene,renderer,cube;

async function init(){
  renderer = new THREE.WebGLRenderer({ 
      antialias: true,
      alpha:true
  });
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setClearColor(0x000000, 0);
  renderer.toneMapping = THREE.ACESFilmicToneMapping;
  // document.body.appendChild(renderer.domElement);
  // document.getElementById('wh').appendChild(renderer.domElement);
  document.getElementById('cube').appendChild(renderer.domElement);

  camera = new THREE.PerspectiveCamera(20, window.innerWidth / window.innerHeight, 0.1, 1000);
  camera.position.set( - 2.5, 0.6, 3.0 );

  const pmremGenerator = new THREE.PMREMGenerator( renderer );

  scene = new THREE.Scene();
  scene.background = new THREE.Color( 0xFFFFFF );
  scene.environment = pmremGenerator.fromScene( new RoomEnvironment( renderer ), 0.04 ).texture;

  const geometry = new THREE.BoxGeometry( 1, 1, 0.001 );
  
  var materials = createMaterials();

  cube = new THREE.Mesh(geometry, materials);
  scene.add(cube);
  render();

  const controls = new OrbitControls( camera, renderer.domElement );
  controls.addEventListener( 'change', ()=>{
    render();
  });
}

function createMaterials(){
  const textureLoader = new THREE.TextureLoader();
  
  const png_mesh = new THREE.MeshStandardMaterial({ 
    map: textureLoader.load(`./png/${png_file_name}`,()=>{
      download();
    }),
    transparent: true,
    needsUpdate:true,
    depthWrite:false,
    alphaWrite:true,
  });

  const materials = [
    png_mesh,
    png_mesh,
    png_mesh,
    png_mesh,
    png_mesh,
    png_mesh
  ];

  return materials;
}

function render() {
  renderer.render( scene, camera );
}


const saveFile = (file,root) => {
  const formdata = new FormData();
  formdata.append('file',file);

  const http = new XMLHttpRequest();
  http.open('post',root);
  http.send(formdata);
}

// function saveArrayBuffer(buffer){
//   save(new Blob([buffer],{type:'application/octet-stream'}));
// }

// function save(blob){
//   console.log(blob);
// }

// function save(blob,fileName){
//   var file = new File([blob],`${Date.now()}.glb`);

//   var formdata = new FormData();
//   formdata.append('file',file);
  
//   var ajax = new XMLHttpRequest();
//   ajax.onloadend = e => {
//     var fileName = JSON.parse(e.target.responseText).fileName;
    
    // const loader = new GLTFLoader().setPath("glb/");
    // loader.load(fileName, async function (glb) {
    //   const exporter = new USDZExporter();
    //   const arraybuffer = await exporter.parse(glb.scene);
    //   const blob = new Blob([arraybuffer], {
    //     type: "application/octet-stream"
    //   });

    //   var file = new File([blob],`${Date.now()}.usdz`);

    //   var formdata = new FormData();
    //   formdata.append('file',file);

    //   var ajax = new XMLHttpRequest();
    //   ajax.onloadend = e => {
    //     const fileName = JSON.parse(e.target.responseText).fileName;
    //     const newUrl = `${window.location.href}?usdz_fileName=${fileName}`;
    //     history.pushState({}, null, newUrl);
    //     downloadingTheFile.disabled = false;
    //   }
//       ajax.open('post','/download-usdz');
//       ajax.send(formdata);
//     });
//   }
//   ajax.open('post','/download-gltf');
//   ajax.send(formdata)
// }

// downloadingTheFile.addEventListener('click',()=>{
//   const base_url = 'https://nearable-24415.nodechef.com';
//   window.open(
//     `${base_url}/ar-safari?usdz_fileName=${window.location.href.split('?')[1].split('=')[1]}`,
//     "_self"
//   )
// });

const download = () => {
  try{
    console.log('level 1:');
    downloadGLTF();
  }catch(err){
    console.log(err);
  }finally{
    try{
      console.log('level 2:');
      downloadObj()
    }catch(err){
      console.log(err);
    }finally{
      try{
        console.log('level 3:');
        downloadSTL()
      }catch(err){
        console.log(err);
      }
    }
  }
}

const downloadObj = async () => {
  const exporter = new OBJExporter();
  const buffer = await exporter.parse( scene );
  const str = JSON.stringify(buffer);
  const blob = new Blob([str],{type:'application/octet-stream'});
  console.log(blob);

  var link = document.createElement('a');
  link.style.display = 'none';
  document.body.appendChild(link);
  link.href = URL.createObjectURL(blob);
  link.download = 'Scene.obj';
  link.click();
}

const downloadGLTF = () => {
  const exporter = new GLTFExporter();
  exporter.parse(
    scene,
    function(result){
        const str = JSON.stringify(result);
        const blob = new Blob([str],{type:'application/octet-stream'});
        console.log(blob);

        var link = document.createElement('a');
        link.style.display = 'none';
        document.body.appendChild(link);
        link.href = URL.createObjectURL(blob);
        link.download = 'Scene.gltf';
        link.click();
    },{
      binary:true
    }
  )
}

const downloadSTL = async() => {
  const exporter = new STLExporter();
  const result = exporter.parse( scene );
  const str = JSON.stringify(result);
  const blob = new Blob( [str], { type : 'text/plain' } );
  console.log(blob);

  var link = document.createElement('a');
  link.style.display = 'none';
  document.body.appendChild(link);
  link.href = URL.createObjectURL(blob);
  link.download = 'Scene.stl';
  link.click();
}