const isiOS12OrNewer = () => {

	const iOS = /iP(hone|od|ad)/.test(navigator.userAgent) && !window.MSStream;
	const iOSVersion = iOS && parseInt(navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/)[1], 10);

	if ( iOS && iOSVersion >= 12 ) { return true; }
	else { return false; }

}

const enhanceWithARQuickLook = () => {

	// if the host device supports AR Quick Look...
	if ( isiOS12OrNewer() ) {

		const attr = 'data-ar-fp',
			elements = document.querySelectorAll('['+attr+']');

		alert(attr)

		// if there are AR-ready links on the page...
		if ( elements.length > 0 ) {

			// convert AR-ready links
			for ( var i in elements ) {

				const instance = elements[i],
					a = document.createElement('a');
				a.setAttribute('href', instance.getAttribute(attr));
				a.setAttribute('rel', 'ar');
				instance.removeAttribute(attr);
				instance.parentNode.insertBefore(a, instance);
				a.appendChild(instance);

			}

		}

	}

};

window.onload = () => {
	var isChrome = (navigator.userAgent.indexOf("Chrome") != -1 && navigator.vendor.indexOf("Google Inc") != -1);
	var main_url = window.location.href.split('?')[0].split('/');
	var redirect = `${main_url[0]}//${main_url[2]}/chrome`;

	const mainUrl = window.location.href.split('?')[1].split('=')[1];
	const decrypted = decrypt("abc",mainUrl);
	const obj = JSON.parse(decrypted);

	redirect = `${redirect}?obj=${mainUrl}`;

	if(isChrome){
		window.open(redirect,"_self");
	}

    const img = document.querySelector('img');
    const usdz_fileName = obj.usdzFileName;
    img.setAttribute('data-ar-fp',`./usdz/${usdz_fileName}`);
    enhanceWithARQuickLook();
}