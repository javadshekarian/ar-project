import * as THREE from 'three';
import { OrbitControls } from 'OrbitControls';
import { USDZExporter } from 'USDZExporter';
import { GLTFExporter } from 'GLTFExporter';
import { RoomEnvironment } from 'RoomEnvironment.js';
import { GLTFLoader } from 'GLTFLoader.js';

const base_url = 'https://nearable-24415.nodechef.com';
// const base_url = 'http://localhost:3000';

const input = document.querySelector('input');
const downloadingTheFile = document.getElementById('downloadingTheFile');
const video = document.getElementById('showVideo');
const mainVideo = document.getElementById('mainVideo');
const canvas = document.querySelector('canvas');
const takePicMenu = document.getElementById('takePicMenu');
const containerThree = document.getElementById('containerThree');
const showingImg = document.getElementById('showingImg');
const move = document.getElementById('move');
const percentRemoveBackground = document.getElementById('percentRemoveBackground');
const shareLink = document.getElementById('shareLink');
const flipCamera = document.querySelector('.fa-camera-rotate');

const space = '&nbsp;&nbsp;&nbsp;&nbsp;';

var png_file_name = '';
var usdz_file_name_for_open = '';
var gltf_fileName = '';
var intPercent = null;
var j = 0;
var webcamStream = '';

let useFrontCamera = true;
let videoStream;

const constraints = {
  video: {
    width: 1280,
    height: 720,
    facingMode:"environment"
  }
};

const removeBackgroundAnim = () => {
  containerThree.classList.remove('dsn');
  intPercent = setInterval(() => {
    ++j;
    if(100<j){
      move.style.width = `100%`;
      percentRemoveBackground.innerHTML = `final check!`;
    }else{
      move.style.width = `${j}%`;
      percentRemoveBackground.innerHTML = `${j}% ${space} Remove Background`;
    }
  }, 300);
}

const endOfremoveBackgroundAnim = () => {
  percentRemoveBackground.innerHTML = `final check!`;
  move.style.width = '100%';
  clearInterval(intPercent);

  setTimeout(() => {
    intPercent = null;
    j=0;

    showingImg.classList.remove('dsn');
    containerThree.classList.add('dsn');
  }, 5*1000);
}

input.onchange = () => {
    percentRemoveBackground.innerHTML = `0% Remove Background`;
    move.style.width = `0%`;
    const file = input.files[0];

    if(file !== undefined){
      showingImg.classList.add('dsn');
      removeBackgroundAnim();
    }

    const formdata = new FormData();
    formdata.append('file',file);

    const http = new XMLHttpRequest();
    http.onloadend = e => {
      const res = JSON.parse(e.target.responseText);
      png_file_name = res.fileName;

      showingImg.style.backgroundImage = `url(png/${png_file_name})`;

      endOfremoveBackgroundAnim();

      init();
    }
    http.open('post','/remove-background');
    http.send(formdata);
}

var camera,scene,renderer;

async function init(){
  renderer = new THREE.WebGLRenderer({ 
      antialias: true,
      alpha:true
  });
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setClearColor(0x000000, 0);
  renderer.toneMapping = THREE.ACESFilmicToneMapping;
  document.getElementById('cube').appendChild(renderer.domElement);

  camera = new THREE.PerspectiveCamera(20, window.innerWidth / window.innerHeight, 0.1, 1000);
  camera.position.set( - 2.5, 0.6, 3.0 );

  const pmremGenerator = new THREE.PMREMGenerator( renderer );

  scene = new THREE.Scene();
  scene.background = new THREE.Color( 0xFFFFFF );
  scene.environment = pmremGenerator.fromScene( new RoomEnvironment( renderer ), 0.04 ).texture;

  const geometry = new THREE.BoxGeometry( 1, 1, 0.001 );
  
  var materials = createMaterials();

  const cube = new THREE.Mesh(geometry, materials);
  scene.add(cube);
  render();

  const controls = new OrbitControls( camera, renderer.domElement );
  controls.addEventListener( 'change', ()=>{
    render();
  });
}

function createMaterials(){
  const textureLoader = new THREE.TextureLoader();
  
  const png_mesh = new THREE.MeshStandardMaterial({ 
    map: textureLoader.load(`./png/${png_file_name}`,()=>{
      download();
    }),
    transparent: true,
    needsUpdate:true,
    depthWrite:false,
    alphaWrite:true,
  });

  const materials = [
    png_mesh,
    png_mesh,
    png_mesh,
    png_mesh,
    png_mesh,
    png_mesh
  ];

  return materials;
}

function render() {
  renderer.render( scene, camera );
}

function download() {
  const exporter = new GLTFExporter();
  exporter.parse(
    scene,
    function(result){
      saveArrayBuffer(JSON.stringify(result),'threejsScene.glb')
    },{
      binary:true
    }
  )
}

function saveArrayBuffer(buffer,fileName){
  save(new Blob([buffer],{type:'application/octet-stream'}),fileName);
}

function save(blob){
  const file = new File([blob],'file.gltf');

  const formdata = new FormData();
  formdata.append('file',file);

  const http = new XMLHttpRequest();
  http.onloadend = e => {
    const response = JSON.parse(e.target.responseText);
    const fileName = response.fileName;
    gltf_fileName = fileName;

    const loader = new GLTFLoader().setPath("gltf/");
    loader.load(fileName, async function (gltf) {
      const exporter = new USDZExporter();
      const arraybuffer = await exporter.parse(gltf.scene);
      const blob = new Blob([arraybuffer], {type: "application/octet-stream"});
      const file = new File([blob],`${Date.now()}.usdz`);



      const fileName = await sendApi(file,'/download-usdz');
      usdz_file_name_for_open = fileName;
      downloadingTheFile.disabled = false;
    });
  }
  http.open('post','/download-gltf-file');
  http.send(formdata);
}

const sendApi = (file,url) => {
  return new Promise(resolve=>{
    var formdata = new FormData();
    formdata.append('file',file);

    const http = new XMLHttpRequest();
    http.onloadend = e => {
      resolve(JSON.parse(e.target.responseText).fileName)
    }
    http.open('post',url);
    http.send(formdata);
  })
}

const getPicture = {
  openARPageFunc : () => {
    const urlObj = {
      usdzFileName:usdz_file_name_for_open,
      gltfFileName:gltf_fileName
    }
  
    const encrypted = crypt("abc", JSON.stringify(urlObj));
  
    var ios_url = `${base_url}/ar-safari?obj=${encrypted}`;
    var chrome_url = `${base_url}/chrome?obj=${encrypted}`;
  
    var isChrome = (navigator.userAgent.indexOf("Chrome") != -1 && navigator.vendor.indexOf("Google Inc") != -1);
    if(isChrome){
      window.open(chrome_url,"_self");
    }else{
      window.open(ios_url,"_self");
    }
  },
  takePictureFunc : async() => {
    showingImg.classList.add('dsn');

    removeBackgroundAnim();

    const ctx = canvas.getContext('2d');
    ctx.drawImage(mainVideo,0,0,1280,720);

    canvas.toBlob(async blob=>{
      // getPicture.autoDownload(blob);

      const file = new File([blob],'file.jpg');

      const fileName = await sendApi(file,'/remove-background');
      endOfremoveBackgroundAnim();
      png_file_name = fileName;

      showingImg.style.backgroundImage = `url(png/${png_file_name})`;

      init();
    })
  },
  shareLinkFunction : async () => {
    const urlObj = {
      usdzFileName:usdz_file_name_for_open,
      gltfFileName:gltf_fileName
    }
  
    const encrypted = crypt("abc", JSON.stringify(urlObj));

    try{
      await navigator.share({
        title: "AR",
        text: "click to see object in AR!",
        url: `${base_url}/chrome?obj=${encrypted}`
      });
    }catch(err){
      console.log(err);
    }
  },
  autoDownload : (blob) => {
    const link = document.createElement('a');
    link.download = 'file.jpg';
    link.href = URL.createObjectURL(blob);
    link.style.display ='none';
    document.body.appendChild(link);
    link.click();
  }
}

takePicMenu.addEventListener('click',()=>getPicture.takePictureFunc());
shareLink.addEventListener('click',()=>getPicture.shareLinkFunction());
downloadingTheFile.addEventListener('click',()=>getPicture.openARPageFunc());

(async function(){
  var isChrome = (navigator.userAgent.indexOf("Chrome") != -1 && navigator.vendor.indexOf("Google Inc") != -1);

  if(!isChrome){
    const allbtnVox = document.getElementById('allbtnVox');
    allbtnVox.classList.add('mt550vi');
  }
  
  video.setAttribute('autoplay', '');
  video.setAttribute('muted', '');
  video.setAttribute('playsinline', '');

  mainVideo.setAttribute('autoplay', '');
  mainVideo.setAttribute('muted', '');
  mainVideo.setAttribute('playsinline', '');


  if (videoStream) {
    videoStream.getTracks().forEach((track) => {
        track.stop();
    });
  }
  constraints.video.facingMode = useFrontCamera ? "user" : "environment";
  
  const exact_amount = "environment";

  videoStream = await navigator.mediaDevices.getUserMedia({
    video:{
      width:1280,
      height:720,
      facingMode:{
        exact:exact_amount
      }
    }
  })

  video.srcObject = videoStream;
  mainVideo.srcObject = videoStream;

  video.play();
  mainVideo.play();
})();

const btnBtns = document.querySelectorAll('.btn-btn');
const see3dObject = document.getElementById('see3dObject');

see3dObject.addEventListener('click',()=>{
  showingImg.classList.remove('dsn');
})

const changeClassFix = {
  takePictureFix:'takePictureGif',
  arFix:'arGif',
  uploadFix:'uploadGif',
  g3dFix:'g3dGif',
  shareFix:'shareGif'
}

btnBtns.forEach(btnBtn=>{
  btnBtn.addEventListener('click',e=>{
    btnBtns[0].classList.remove('uploadGif');
    btnBtns[1].classList.remove('arGif');
    btnBtns[2].classList.remove('takePictureGif');
    btnBtns[3].classList.remove('g3dGif');
    btnBtns[4].classList.remove('shareGif');

    btnBtns[0].classList.remove('gif');
    btnBtns[1].classList.remove('gif');
    btnBtns[2].classList.remove('gif');
    btnBtns[3].classList.remove('gif');
    btnBtns[4].classList.remove('gif');

    btnBtns[0].classList.add('uploadFix');
    btnBtns[1].classList.add('arFix');
    btnBtns[2].classList.add('takePictureFix');
    btnBtns[3].classList.add('g3dFix');
    btnBtns[4].classList.add('shareFix');

    btnBtns[0].classList.add('not-gif');
    btnBtns[1].classList.add('not-gif');
    btnBtns[2].classList.add('not-gif');
    btnBtns[3].classList.add('not-gif');
    btnBtns[4].classList.add('not-gif');

    const target = e.target;
    const classList = target.classList;
    const gifName = changeClassFix[classList[1]];
    const fixName = classList[1];

    target.classList.remove(fixName);
    target.classList.remove('not-gif');

    target.classList.add(gifName);
    target.classList.add('gif');
  })
})