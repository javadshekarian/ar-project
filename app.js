const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const upload = require('express-fileupload');
const {generate} = require('shortid');
const {removeBackground} = require('@imgly/background-removal-node');

const {uploadBuffer} = require('./app/helper');

const app = express();

app.use(express.static(path.join(__dirname,'public')));
app.use(bodyParser.urlencoded({extended:false}));
app.use(upload());

app.get('/',(req,res)=>{
    res.sendFile(
        path.join(__dirname,'public','index.html')
    );
});

app.get('/ar-safari',(req,res)=>{
    res.sendFile(

    path.join(__dirname,'public','ar-safari.html')
    );
});

app.get('/test',(req,res)=>{
    res.sendFile(
    path.join(__dirname,'public','test.html')
    );
});

app.get('/chrome',(req,res)=>{
    res.sendFile(
        path.join(__dirname,'public','chrome.html')
    );
});

app.get('/btns',(req,res)=>{
    res.sendFile(
        path.join(__dirname,'public','btns.html')
    );
});

app.post('/remove-background',(req,res)=>{
    const file = req.files.file.data;
    const fileName = `${generate()}.png`;
    const webmFileName = `${generate()}.webm`;

    removeBackground(file).then(async blob=>{
        const arrayBuffer = await blob.arrayBuffer();
        const buff = Buffer.from(arrayBuffer);

        await uploadBuffer(fileName,buff);
        
        res.json({
            fileName,
            webmFileName
        })
    })
});

app.post('/download-gltf-file',(req,res)=>{

    const file = req.files.file;
    const fileName = `${generate()}.gltf`;


    file.mv(`./public/gltf/${fileName}`,err=>{
        if(err){
            console.log(err);
        }
        res.json({fileName:fileName});
    })
});

app.post('/download-usdz',(req,res)=>{
    const file = req.files.file;
    const fileName = `${generate()}.usdz`;

    file.mv(`./public/usdz/${fileName}`,err=>{
        if(err){
            console.log(err);
        }
        res.json({fileName:fileName});
    })
})

// app.listen(process.env.PORT||3000,()=>{
// 	console.log(`the app is listen to port ${process.env.PORT||3000}!`)
// })

module.exports.app = app;