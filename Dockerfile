FROM node:latest

WORKDIR /usr/node-rembg

COPY . .

RUN npm install

CMD [ "node","server.js" ]